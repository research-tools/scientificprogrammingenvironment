# Set up scientific computing environment in macOS (version 10.13.6)

##  Install Xcode
1. Download and install
2. After installing Xcode, install command line tools from Apple;
   https://developer.apple.com/download/more/

3. ```$ sudo xcodebuild -license```  
   (It will show your license if it is installed correctly)

## Install homebrew 
1. Download from https://brew.sh/
2. Install

## Install emacs
1. ```$ brew install --with-cocoa emacs```
2. Copy elisp settings (elisp, site-start.d, and init.el) to /home/(username)/.emacs.d
3. Set up emacs
   - My settings (https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/Emacs/SetupEmacs.md)

## Install gcc (gfortran)
1. ```brew install gcc```
2. Create symbolic links (You should check the version)
```
ln -s /usr/local/bin/gcc-8 /usr/local/bin/gcc
ln -s /usr/local/bin/g++-8 /usr/local/bin/g++
```

## Install Intel MKL
1. Download Intel MKL
2. Install

## Install Intel Python Distribution (3.X)
1. Change directory to home (/User/hiromi)
2. Download and extract files to /User/hiromi
3. ```$ bash setup_intel_python.sh```
4. ```$ source ./bin/activate```
5. After install, create .bash_profile as follows:  
   ```$ emacs .bash_profile```  
   and add
```
# .bash_profile                                                                 

# Run .bashrc                                                                   
if [ -f ~/.bashrc ]; then
  . ~/.bashrc
fi
```
5. Also, create .bashrc    
```$ emacs .bashrc```  
	and add  
	```export PATH=/User/hiromi/intelpython3/bin:$PATH```
	
6. Create multiple environments
   (Example: Environment for python 3.6)
   - Create a new environement  
	   ```$ conda create -n env_py36 python=3.6```  
     or
   - Copy root  
	   ```$ conda list --explicit > env_root.txt```  
	   ```$ sudo conda create -n env_ml --file env_root.txt```

   - Switch environments:  
	   ```$ source activate env_py36```
     or
		```$ source deactivate```
   - Install packages in this environment:
		```$ conda install numpy -n env_py36```
   -  Check your environments:  
		```$ conda info -e```


## Install numpy, spicy, matplotlib, and spyder
1. ```$ conda search spyder```
2. ```$ sudo conda install spyder```
3. modify matplotlib setting
   - Preferences > IPython Console > Graphics > Backend, and change to automatic

## Install Paraview
1. Download from https://www.paraview.org/
2. Install

## Install openMPI
1. Download and extract
   - ```$ cd (directory)```
   - ```$ ./configure --prefix=/usr/local/bin CC=clang CXX=clang++ F77=gfortran FC=gfortran CFLAGS=-m64 CXXFLAGS=-m64 FCFLAGS=-m64 FFLAGS=-m64```
   - ```$ make -j2 all```
   - ```$ sudo make all install```
2. Modify .bashrc by adding the following sentences
```
export PATH=/usr/local/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export MANPATH=/usr/local/share/man:$MANPATH
```

## Install openCV
1. ```$ brew install opencv3 --with-python3 --without-python --with-contrib```
2. Create a soft link
   - ```$ cd /Users/hiromi/intelpython3/envs/env_py36/lib/python3.6/site-packages```
   - ```$ sudo ln -s /usr/local/Cellar/opencv/3.4.2/lib/python3.7/site-packages/cv2.cpython-37m-darwin.so cv2.so```

1. ```brew install pkg-config jpeg eigen webp ffmpeg jasper libpng libtiff openexr gstreamer libgphoto2 eigen ilmbase imagemagick gflags glog ceres-solver```
2. ```brew uninstall vtk```
3. ```brew install vtk --with-python3 --without-python--with-qt```
4. Download opencv and extract files in Download  
https://opencv.org/releases.html
https://github.com/opencv/opencv_contrib/releases
5. After extract files,   
```cp -r opencv_contrib-3.4.1 opencv-3.4.1/contrib```
6. ```cd opencv-3.4.1```
7. ```mkdir build```
8. ```cd build```
9. Use cmake  
```
cmake -D CMAKE_CXX_COMPILER=clang++ -D CMAKE_C_COMPILER=clang \
    -D CMAKE_CXX_FLAGS=-std=gnu++14 \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.4.3/modules \
    -D MKL_LAPACKE_INCLUDE_DIR=/opt/intel/compilers_and_libraries_2019.0.117/mac/mkl/include \
    -D PYTHON_EXECUTABLE=/Users/hiromi/intelpython3/envs/env_py36/bin/python \
    -D PYTHON3_LIBRARY=/Users/hiromi/intelpython3/envs/env_py36/lib/libpython3.6m.dylib \
    -D PYTHON3_INCLUDE_DIR=/Users/hiromi/intelpython3/envs/env_py36/include/python3.6m \
    -D PYTHON3_EXECUTABLE=/Users/hiromi/intelpython3/envs/env_py36/bin/python \
    -D BUILD_opencv_python2=OFF \
    -D BUILD_opencv_python3=ON \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D WITH_MATLAB=OFF \
    -D WITH_QT=ON \
    -D WITH_OPENGL=OFF \
    -D ENABLE_PRECOMPILED_HEADERS=OFF \
    -D BUILD_TESTS=OFF \
    -D BUILD_EXAMPLES=ON ..
```
10. ```make -j2```
11. ```sudo make install```
12. Sym-link our openCV (cv2.so) bidings into our environment (my case: env_py36)   
   - ```$ ln -s /usr/local/lib/python3.6/site-packages/cv2.cpython-36m-darwin.so /Users/hiromi/intelpython3/envs/env_py36/lib/python3.6/site-packages/cv2.so```


## Install Latex
1. Download from http://www.tug.org/mactex/mactex-download.html
2. Install

## Scientific library for C/C++ and Fortran
1. Install GSL (GNU Scientific Library; https://www.gnu.org/software/gsl/)
	- Download
	- ```$ cd (directory)```
	- ```$ ./configure CC=clang FC=gfortran```
	- ```$ make```
	- ```$ sudo make install```   
	(If something wrong, 'make uninstall' to uninstall everything)
2. Install FGSL (Fortran interface to GSL; https://www.lrz.de/services/software/mathematik/gsl/fortran/)
	- Download
	- ```$ cd (directory)```
	- ```$ gsl_CFLAGS=-I/usr/local/include gsl_LIBS=-L/usr/local/lib ./configure CC=clang FC=gfortran```
	- ```$ make```
	- ```$ sudo make install```
	- To compile a Fortran code;   
		```gfortran -O3 -I/usr/local/include/fgsl -L/usr/local/lib -lgsl -lfgsl -lgslcblas -shared -fPIC ....```


## Install tensorflow
1. ```$ conda install scikit-learn```
2. ```$ conda install tensorflow```

## Install pybullet
1. Download and extract in /home
2. ```$ ./build_cmake_pybullet_double.sh```
3. Modify bashrc  
   ```export PYTHONPATH=/Users/hiromiyasuda/bullet3-master/build_cmake/examples/pybullet```

## Install inkscape
1. Use installer (XQuartz is reuired)

## Install TISEAN
1. ```$ brew tap brewsci/science```
2. ```$ brew install tisean```
3. When you use it in macOS, you need to add 'tisean-' infront of the command
	
## Upgrade to Mojave
1. Go to /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14. pkg
2. Install
