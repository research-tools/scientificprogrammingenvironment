# Set up Ubuntu 20.04 LTS (2021/04/30)
## Dell XPS 8940

## Clean install
1. Download Ubuntu
2. Create a USB boot (I used Rufus)
3. Restart the computer and press F12 to choose USB boot

## Set up Japanese input
1. Install ibus-mozc
```
$ sudo apt-get install ibus-mozc
```
2. Restart ibus
```
$ killall ibus-daemon
$ ibus-daemon -d -x &
```
3. Go to "Settings" => "Region & Language" => Input Sources => Japanese => Select "Japanese (Mozc)"

## Install Anaconda
1. ```$ sudo apt update```
1. Download Anaconda
1. ```$ bash Anaconda3-2020.07-Linux-x86_64.sh```
1. Follow the instructions to install Anaconda
   * License: "yes"=>"Enter"
   * Conda init: "yes"=>"Enter"
<!-- 1. Change the base Python version to 3.7 (3.8 doesn't work for Code_Aster 14.6)
```
$ conda install python=3.7
```
(This takes about 30 minutes...) -->
1. Create a new environment and Install numpy, scipy, matplotlib, spyder
	- ```$ conda create -n env_py38 python=3.8```
	- ```$ source activate env_py38```
	- ```$ conda install numpy scipy matplotlib spyder python=3.8```

## Install emacs via apt-get
1. ```$ sudo apt-get install emacs25```
2. Copy elisp settings (elisp, site-start.d, and init.el) to /home/(username)/.emacs.d
3. Set up emacs
   1. Add package-archives: ```M-x package-list-packages```

   2. Install "company" for text completion
      - Find package: ```M-x package-list-packages```
      - Search: ```C-s company```
      - Install:
         - bring the cursor to the package "company"
         - type: 'i' and 'x'

   3. Install "yasnippet"
      - Find package: ```M-x package-list-packages```
      - Search: ```C-s yasnippet```
      - Install:
         - bring the cursor to the package "yasnippet"
         - type: 'i' and 'x'
      - Search: ```C-s yasnippet-snippets```
      - Install:
         - bring the cursor to the package "yasnippet-snippets"
         - type: 'i' and 'x'
      - Add the following lines to init.el:
      ```
      (add-to-list 'load-path
               (expand-file-name "~/.emacs.d/elpa/yasnippet-20200604.246/yasnippet"))

      (require 'yasnippet)
      (setq yas-snippet-dirs
         '("~/.emacs.d/mySnippets" 
         "~/.emacs.d/elpa/yasnippet-snippets-20210408.1234/snippets"
         ))
      (yas-global-mode 1)
      ```
   4. Install "flymake-cursor" for text completion
      - Find package: ```M-x package-list-packages```
      - Search: ```C-s flymake-cursor```
      - Install:
         - bring the cursor to the package "flymake-cursor"
         - type: 'i' and 'x'

   5. Install "markdown-mode" for text completion
      - Find package: ```M-x package-list-packages```
      - Search: ```C-s markdown-mode```
      - Install:
         - bring the cursor to the package "markdown-mode"
         - type: 'i' and 'x'

   6. My settings (https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/Emacs/SetupEmacs.md)

## Install Visual Studio Code
1. Install via Ubuntu Software
2. File -> Preferences -> Keymaps -> Install "Emacs Keymap" (For Emacs key bindings)
3. Edit "Keyboard Shortcuts" (Paste(default): "Ctrl+v" => "Ctrl+y" because of Emacs keybinding)
4. Install "Code Spell Checker" (extension)
5. Install "Markdown+Math" (extension)

## Set up programming environment
1. ```$ sudo apt install build-essential```

## Set up Latex environment (Tex Live and VSCode)
(Based on https://qiita.com/rainbartown/items/d7718f12d71e688f3573)
1. ```$ sudo apt install texlive-full```
2. Create ".latexmkrc" under ~/ (Home/hiromi): See my example
3. Go to "Extensions" -> Install "LaTex Workshop" (James Yu)
4. Click "Manage" (Gear icon at the bottom left) -> "Settings" -> Click "Open Settings (JSON)" (File icon at top right) -> Edit ``settings.json`` as follows: 
```
{
    // ---------- Language ----------

    "[tex]": {
        // スニペット補完中にも補完を使えるようにする
        "editor.suggest.snippetsPreventQuickSuggestions": false,
        // インデント幅を2にする
        "editor.tabSize": 2
    },

    "[latex]": {
        // スニペット補完中にも補完を使えるようにする
        "editor.suggest.snippetsPreventQuickSuggestions": false,
        // インデント幅を2にする
        "editor.tabSize": 2
    },

    "[bibtex]": {
        // インデント幅を2にする
        "editor.tabSize": 2
    },


    // ---------- LaTeX Workshop ----------

    // 使用パッケージのコマンドや環境の補完を有効にする
    "latex-workshop.intellisense.package.enabled": true,

    // 生成ファイルを削除するときに対象とするファイル
    // デフォルト値に "*.synctex.gz" を追加
    "latex-workshop.latex.clean.fileTypes": [
        "*.aux",
        "*.bbl",
        "*.blg",
        "*.idx",
        "*.ind",
        "*.lof",
        "*.lot",
        "*.out",
        "*.toc",
        "*.acn",
        "*.acr",
        "*.alg",
        "*.glg",
        "*.glo",
        "*.gls",
        "*.ist",
        "*.fls",
        "*.log",
        "*.fdb_latexmk",
        "*.snm",
        "*.nav",
        "*.dvi",
        "*.synctex.gz"
    ],

    // 生成ファイルを "out" ディレクトリに吐き出す
    "latex-workshop.latex.outDir": "out",

    // ビルドのレシピ
    "latex-workshop.latex.recipes": [
        {
            "name": "latexmk",
            "tools": [
                "latexmk"
            ]
        },
    ],

    // ビルドのレシピに使われるパーツ
    "latex-workshop.latex.tools": [
        {
            "name": "latexmk",
            "command": "latexmk",
            "args": [
                "-silent",conda install python=3.6
            ],
        },
    ],
}

```
5. To build a tex file, ```ctrl + alt + b```



## Install Inkscape
(Don't use "Ubuntu Software")
1. ```$ sudo apt remove inkscape```
2. ```$ sudo add-apt-repository ppa:inkscape.dev/stable```
3. ```$ sudo apt update```
4. Install Inkscape 1.0
```$ sudo apt install inkscape```

## Install Git
1. ```$ sudo apt install git```
1. ```$ git config --global user.name "hiromiyasuda"```
1. ```$ git config --global user.email "~~~"```

## Install Salome-meca 2020
(This is based on https://hitoricae.com/2021/02/11/xubuntu20-04lts%e3%81%absalome_meca2020%e3%82%92%e3%82%a4%e3%83%b3%e3%82%b9%e3%83%88%e3%83%bc%e3%83%ab/)
1. Download salome_meca-2020.0.1-1-universal.tgz from the website(https://www.code-aster.org/V2/spip.php?article303)
1. Extract this file (generate salome_meca-2020.0.1-1-universal.run)
1. ```＄sudo apt-get install qtbase5-dev qttools5-dev-tools qt5-default net-tools libnlopt0```
1. ```$ sudo apt-get install libffi* ```
1. ```$ ./salome_meca-2020.0.1-1-universal.run```
1. To activate AsterStudy
```
$ cd ~/salome_meca/V2020.0.1_universal_universal/prerequisites/debianForSalome/lib/
$ mkdir kari
$ mv libstdc++* kari
$ ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6 libstdc++.so.6
$ ln -s /usr/lib/x86_64-linux-gnu/libffi.so libffi.so.6
```
1. Add the following lines in .bashrc file
```
# Salome-meca
export PATH=/home/hiromi/salome_meca/appli_V2020.0.1_universal_universal:$PATH
```

## Install Code_Aster (Version 14.6; Parallel)
This installation section is based on  
- https://hitoricae.com/2020/10/31/code_aster-14-6-parallel-version-with-petsc/
- https://qiita.com/hiro_kuramoto/items/269ecf6293cfbe38b314  
<br />

OS: Ubuntu 20.04 LTS  
Python: 3.8.5 (Anaconda, base)  
Python2: 2.7.18 (apt) 

| Package    | Version | Link |  
| ---------  | ------- | --- |  
| Code_Aster | 14.6   | https://www.code-aster.org/ |
| OpenBLAS   | 3.15   |  |
| ScaLAPACK  | 2.1.0  | http://www.netlib.org/scalapack/ |
| ParMETIS   | 4.0.3  | http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download |
| MUMPs      | 5.1.2  | From aster-full-src-14.6.0-1.noarch.tar.gz  |
|Petsc       | 3.10.5 | https://www.mcs.anl.gov/petsc/download/index.html |
<br />

1. To install packages in /opt, change the owner of /opt
```
$ sudo chown username /opt  (username should be replaced)
```
(When I first tried to install Code_Aster in home/hiromi, failed to build MUMPS, so you should use /opt!)  

1. Set up Python
<!-- - Create a symlink to python-config (original python-config is linked to python2)
```
$ cd cd ~/anaconda3/bin/
$ ln -s python3-config python-config
$ conda install boost -->
$ conda install bison flex zlib
```
1. Install required packages via apt
- Go to Downloaded location  
```
$ sudo apt install gfortran g++ python-dev python-numpy liblapack-dev libblas-dev tcl tk zlib1g-dev checkinstall openmpi-bin libx11-dev cmake grace gettext libboost-all-dev swig libsuperlu-dev
```
1. Install OpenBLAS
- Download tar file from the website
- Install as follows:
```
$ tar xfvz OpenBLAS-0.3.15.tar.gz
$ cd OpenBLAS-0.3.15/
$ make NO_AFFINITY=1 USE_OPENMP=1
$ make PREFIX=/opt/OpenBLAS install
$ echo /opt/OpenBLAS/lib | sudo tee -a /etc/ld.so.conf.d/openblas.conf
$ sudo ldconfig
```

1. Build Code_Aster (sequential version) with OpenBLAS
- Download the package from the website
- Extract the tar file
```
$ tar xvzf aster-full-src-14.6.0-1.noarch.tar.gz
$ cd aster-full-src-14.6.0
```
- Modify "setup.cfg" file as follows:
```
$ sed -i "s:PREFER_COMPILER\ =\ 'GNU':PREFER_COMPILER\ =\'GNU_without_MATH'\nMATHLIB=\ '/opt/OpenBLAS/lib/libopenblas.a'\nLDFLAGS=\ '-fopenmp -fno-lto':g" setup.cfg
```
- Install Code_Aster
```
$ export MKL_SERVICE_FORCE_INTEL=1
$ python3 setup.py install
```
- Run a test case
```
$ /opt/aster/bin/as_run --vers=14.6 --test forma01a
```
- Create a host file for clustering with other computers
```
$ echo "$HOSTNAME cpu=$(cat /proc/cpuinfo | grep processor | wc -l)" > /opt/aster/etc/codeaster/mpi_hostfile 
```
1. Install ScaLAPACK5.1.2r```
- ```$ ./setup.py --lapacklib=/opt/OpenBLAS/lib/libopenblas.a --mpicc=mpicc --mpif90=mpif90 --mpiincdir=/usr/lib/x86_64-linux-gnu/openmpi/include --ldflags_c=-fopenmp --ldflags_fc=-fopenmp --prefix=/opt/scalapack```
- When I tried to run './setup.py' for the first time, it failed to download "scalapack.tgz". So you need to manually download "scalapack-2.1.0.tgz" -> rename it as "scalapack.tgz" -> move it to ~/Downloads/scalapack_installer/build/download 
- Based on https://sites.google.com/site/codeastersalomemeca/home/code_asterno-heiretuka/parallel-code_aster-11-7?tmpl=%2Fsystem%2Fapp%2Ftemplates%2Fprint%2F&showPrintDialog=1#TOC-Compile-ScaLAPACK;
```
An error message "BLACS: error running BLACS test routines xCbtest" will show up after compilation.
But you succeed , if there is an file "/opt/scalapack/lib/libscalapack.a".

1. Instal ParMETIS (4.0.3)
- Download tar.gz file from the website (http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download)
- Extract it and ```$ cd parmetis-4.0.3```
- Modify metis/include/metis.h file as follows:
```
$ sed -i -e 's/#define IDXTYPEWIDTH 32/#define IDXTYPEWIDTH 64/' metis/include/metis.h
```
- Install
```
$ make config prefix=/opt/parmetis-4.0.3 
$ make
$ make install
```
- Check
```
$ cd Graphs
$ mpirun -np 4 /opt/parmetis-4.0.3/bin/parmetis rotor.graph 1 6 1 1 6 1
```
(If no error, succeeded!)

1. Install Scotch (6.0.4)
- Go to aster-full-src-14.6.0/SRC -> Move "scotch-6.0.4-aster7.tar.gz" to /opt -> Extract
- ```$ cd scotch-6.0.4/src```
- Modify /src/Makefile.inc as follows:
```
EXE     =
LIB     = .a
OBJ     = .o

MAKE    = make
AR      = ar
ARFLAGS = -ruv
CAT     = cat
CCS     = gcc
CCP     = mpicc
CCD     = gcc
CFLAGS  = -O3 -fPIC -DINTSIZE64 -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD -DCOMMON_RANDOM_FIXED_SEED -DSCOTCH_RENAME -DSCOTCH_RENAME_PARSER -Drestrict=__restrict
CLIBFLAGS   =
LDFLAGS = -fPIC -lz -lm -pthread -lrt
CP      = cp
LEX     = flex -Pscotchyy -olex.yy.c
LN      = ln
MKDIR   = mkdir
MV      = mv
RANLIB  = ranlib
YACC    = bison -pscotchyy -y -b y 
```
- Install
```
$ make scotch esmumps ptscotch ptesmumps CCD=mpicc
$ make check
$ make ptcheck
```  
(I got an error message after I run "make check" but it should be fine? 2021/05/08)

1. Install MUMPS (5.1.2)
- Go to aster-full-src-14.6.0/SRC -> Move "mumps-5.1.2-aster7.tar.gz" to /opt -> Extract
- Rename as "mumps-5.1.2_mob''
- ```$ cd mumps-5.1.2_mob```
- Create a Makefile.inc by using a template file as follows:
```
$ cp Make.inc/Makefile.debian.PAR ./Makefile.inc
```  

Then modify this file as follows:  

```
#  This file is part of MUMPS 5.1.2, released
#  on Mon Oct  2 07:37:01 UTC 2017
# These settings for a PC underPetsc (3.10.5)
- Download tar.gz file (Version 3.10.5) from the website (https://www.mcs.anl.gov/petsc/download/index.html)
# apt-get install libmetis-dev libparmetis-dev libscotch-dev libptscotch-dev libatlas-base-dev openmpi-bin libopenmpi-dev lapack-dev

# Begin orderings
LSCOTCHDIR = /opt/scotch-6.0.4/lib/
ISCOTCH    = -I/opt/aster/public/metis-5.1.0/include -I/opt/parmetis-4.0.3/include -I/opt/scotch-6.0.4/include
LSCOTCH   = -L$(LSCOTCHDIR) -lptesmumps -lptscotch -lscotch -lptscotcherr -lptscotcherrexit -lptscotchparmetis

LPORDDIR = $(topdir)/PORD/lib/
IPORD    = -I$(topdir)/PORD/include/
LPORD    = -L$(LPORDDIR) -lpord

LMETISDIR = /opt/parmetis-4.0.3/lib/
IMETIS    = -I/opt/parmetis-4.0.3/include/
LMETIS    = -L$(LMETISDIR) -L/opt/aster/public/metis-5.1.0/lib -lparmetis -lmetis

# Corresponding variables reused later
ORDERINGSF = -Dmetis -Dpord -Dparmetis -Dscotch -Dptscotch
ORDERINGSC  = $(ORDERINGSF)

LORDERINGS = $(LMETIS) $(LPORD) $(LSCOTCH)
IORDERINGSF = $(ISCOTCH)
IORDERINGSC = $(IMETIS) $(IPORD) $(ISCOTCH)
# End orderings
################################################################################
PLAT    =
LIBEXT  = .a
OUTC    = -o 
OUTF    = -o 
RM = /bin/rm -f
CC = mpicc
FC = mpif90
FL = mpif90
AR = ar vr 
RANLIB = echo
LAPACK = /opt/OpenBLAS/lib/libopenblas.a
SCALAP = /opt/scalapack/lib/libscalapack.a
INCPAR = -I/usr/lib/x86_64-linux-gnu/openmpi/include 
LIBPAR = $(SCALAP) $(LAPACK) -L/usr/lib/x86_64-linux-gnu/openmpi/lib -lmpi
INCSEQ = -I$(topdir)/libseq
LIBSEQ  = $(LAPACK) -L$(topdir)/libseq -lmpiseq
LIBBLAS = -L/opt/OpenBLAS/lib -lopenblas
LIBOTHERS = -L/usr/lib/x86_64-linux-gnu -lpthread -lutil -ldl

#Preprocessor defs for calling Fortran from C (-DAdd_ or -DAdd__ or -DUPPER)
CDEFS   = -DAdd_

#Begin Optimized options
# uncomment -fopenmp in lines below to benefit from OpenMP
OPTF    = -O -fPIC -DPORD_INTSIZE64 -fopenmp
OPTL    = -O -fopenmp
OPTC    = -O -fPIC -fopenmp
#End Optimized options

INCS = $(INCPAR)
LIBS = $(LIBPAR)
LIBSEQNEEDED =
```
- Install
```
$ make all
```
- Check
```
$ cd examples
$ mpirun -np 4 ./ssimpletest < input_simpletest_real
```
1. Install Petsc (3.10.5)
- Download tar.gz file (Version 3.10.5) from the website (https://www.mcs.anl.gov/petsc/download/index.html)
- Extract it in /opt
- ```$ cd /opt/petsc-3.10.5```
- Modify /opt/petsc-3.10.5/config/BuildSystem/config/packages/metis.py as follows:
(Comment out Lines 50-55)
```
  def configureLibrary(self):
    config.package.Package.configureLibrary(self)
    oldFlags = self.compilers.CPPFLAGS
    self.compilers.CPPFLAGS += ' '+self.headers.toString(self.include)
    # if not self.checkCompile('#include "metis.h"', '#if (IDXTYPEWIDTH != '+ str(self.getDefaultIndexSize())+')\n#error incompatible IDXTYPEWIDTH\n#endif'):
    #   if self.defaultIndexSize == 64:
    #     msg= '--with-64-bit-indices option requires a metis build with IDXTYPEWIDTH=64.\n'
    #   else:
    #     msg= 'IDXTYPEWIDTH=64 metis build appears to be specified for a default 32-bit-indices build of PETSc.\n'
    #   raise RuntimeError('Metis specified is incompatible!\n'+msg+'Suggest using --download-metis for a compatible metis')

    self.compilers.CPPFLAGS = oldFlags
    return
```
- Add OpenMPI library to LD_LIBRARY_PATH
```
$ export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/openmpi/lib/:$LD_LIBRARY_PATH
```
- Download "hypre_2.14.0.orig.tar.gz" from https://launchpad.net/ubuntu/+source/hypre/2.14.0-5build1
- Move this hypre file to /opt
- Configure
```
$ ./configure --with-debugging=0 COPTFLAGS=-O CXXOPTFLAGS=-O FOPTFLAGS=-O --with-shared-libraries=0 --with-scalapack-dir=/opt/scalapack --PETSC_ARCH=linux-metis-mumps --with-metis-dir=/opt/aster/public/metis-5.1.0 --with-parmetis-dir=/opt/parmetis-4.0.3 --with-ptscotch-dir=/opt/scotch-6.0.4 --LIBS="-lgomp" --with-mumps-dir=/opt/mumps-5.1.2_mob -with-x=0 --with-blas-lapack-lib=[/opt/OpenBLAS/lib/libopenblas.a] --download-hypre=/opt/hypre_2.14.0.orig.tar.gz --download-ml=yes 
```
- Make
```
$ make PETSC_DIR=/opt/petsc-3.10.5 PETSC_ARCH=linux-metis-mumps all
$ make PETSC_DIR=/opt/petsc-3.10.5 PETSC_ARCH=linux-metis-mumps check
```

1. Install Code_Aster (Parallel version)
- Modify /opt/aster/etc/codeaster/asrun by using a text editor as follows:
```
mpi_get_procid_cmd : echo $OMPI_COMM_WORLD_RANK
```
- Go to aster-full-src-14.6.0/SRC -> Move "aster-14.6.0.tgz" to /opt -> Extract
- ```$ cd aster-14.6.0.tgz```
- Modify /opt/aster-14.6.0/waftools/mathematics.py as follows:  
(Comment out the following three lines to skip blacs_pinfo)
```
# program testing a blacs call, output is 0 and 1
blacs_fragment = r"""
program test_blacs
    integer iam, nprocs
    ! call blacs_pinfo (iam, nprocs)
    ! print *,iam
    ! print *,nprocs
end program test_blacs
"""
```
- Create "Ubuntu_gnu_mpi.py" and "Ubuntu_gnu.py" and put them in /opt/aster-14.6.0
(See my python files)
- Build
```
$ export ASTER_ROOT=/opt/aster
$ export PYTHONPATH=/$ASTER_ROOT/lib/python3.8/site-packages/:$PYTHONPATH
$ ./waf configure --use-config-dir=$ASTER_ROOT/14.6/share/aster --use-config=Ubuntu_gnu_mpi --prefix=$ASTER_ROOT/PAR14.6MUPT LINKFLAGS=-fno-lto
```
(Run the last line, it will fail)
- Modify "/opt/aster-14.6.0/.waf3-2.0.20-df7687050314fa98c5daa534cb234b8c/waflib/Tools/c_config.py" as follows:  
(Delete "-flto")
```
elif x.startswith('+')or x in('-pthread','-fPIC','-fpic','-fPIE','-fpie','-fno-lto'):
			app('CFLAGS',x)
			app('CXXFLAGS',x)
			app('LINKFLAGS',x)
```
- ```$ ./waf install -p --jobs=1```
- After 'install' is done, modify '/opt/aster/etc/codeaster/aster' as follows:  
(Add the last line)
```
# Code_Aster versions
# versions can be absolute paths or relative to ASTER_ROOT
# examples : NEW11, /usr/lib/codeaster/NEW11

# default version (overridden by --vers option)
default_vers : stable

# available versions
# DO NOT EDIT FOLLOWING LINE !
#?vers : VVV?
vers : stable:/opt/aster/14.6/share/aster
vers : 14.6MUPT:/opt/aster/PAR14.6MUPT/share/aster
```
- Add the following lines in ./bashrc
```
export ASTER_ROOT=~/aster
export PATH=/opt/aster/bin:$PATH
```


## Install Paraview
**Using apt tool**
1. ```$ sudo apt install paraview```
1. ```$ sudo apt install libcanberra-gtk-module```

**From the source code**  
(Based on https://gitlab.kitware.com/paraview/paraview/blob/master/Documentation/dev/build.md)
1. Dependencies
```
$ sudo apt-get install git cmake build-essential libgl1-mesa-dev libxt-dev qt5-default libqt5x11extras5-dev libqt5help5 qttools5-dev qtxmlpatterns5-dev-tools libqt5svg5-dev python3-dev python3-numpy libopenmpi-dev libtbb-dev ninja-build
```
1. Build
```
$ mkdir Paraview
$ cd Paraview
$ git clone --recursive https://gitlab.kitware.com/paraview/paraview.git
$ mkdir paraview_build
$ cd paraview_build
$ cmake -GNinja -DPARAVIEW_USE_PYTHON=ON -DCMAKE_BUILD_TYPE=Release ../paraview
$ ninja
```
1. Add the path to paraview in .bashrc
```
export PATH=$PATH:$HOME/Paraview/paraview_build/bin
```


## Install Elmer
1. Go to "Software & Updates" and change to "Download from: Main server"  
("Server for Japan didn't work)
1.  Use csc package
```
$ sudo apt-add-repository ppa:elmer-csc-ubuntu/elmer-csc-ppa
$ sudo apt-get update
$ sudo apt-get install elmerfem-csc-eg
$ sudo apt install libqt5xml5
```
1. Add additional equations (models)  
(Example: Nonlinear elasticity, Elatic plate, and Shell solver)
```
$ sudo cp /usr/share/ElmerGUI/edf-extra/nonlinearelasticity.xml /usr/share/ElmerGUI/edf
$ sudo cp /usr/share/ElmerGUI/edf-extra/elasticplate.xml /usr/share/ElmerGUI/edf
$ sudo cp /usr/share/ElmerGUI/edf-extra/shellsolver.xml /usr/share/ElmerGUI/edf
$ sudo cp /usr/share/ElmerGUI/edf-extra/savescalars.xml /usr/share/ElmerGUI/edf
```

## Install Node.js and NPM
```
$ sudo apt install nodejs npm
```

## Install MKdocs to manage documents
```
$ conda install -c conda-forge mkdocs
$ conda install -c conda-forge python-markdown-math 
$ conda install -c conda-forge mkdocs-material
```
(I installed it in "base")

## Install Intel compilers
(https://software.intel.com/content/www/us/en/develop/articles/free-intel-software-developer-tools.html)  
1. Preparation
```
$ wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB
$ sudo apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB
$ echo "deb https://apt.repos.intel.com/oneapi all main" | sudo tee /etc/apt/sources.list.d/oneAPI.list
$ sudo apt update
```
1. Then install
```
$ sudo apt install intel-basekit
$ sudo apt install intel-hpckit
```
- Packages will be installed at ```/opt/intel/oneapi```
1. Set up
- Add the following lines in /home/.bashrc
```
source /opt/intel/oneapi/setvars.sh
```

## Install OpenShot (Video editor)
1. Download AppImage (https://www.openshot.org/)
1. Right click on the AppImage file -> select ``Properties``
1. Go to ``Permissions`` and check ``Allow executing file as program``