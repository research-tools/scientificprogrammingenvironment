# Set up Emacs for C++ editor
- My init.el file is here; https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/Emacs/init.el
- Place this file in "/home/.emacs.d"

## Installation
- For Linux:
	- Use apt-get
- For MacOS:
	- Use homebrew to install   
		```$ brew install --with-cocoa emacs```  
		(macOS 10.14, Mojave) ```$ brew cask install emacs```

## Install auto-complete
1. Find package:
   - type: ```M-x package-list-packages```
2. Search:
   - type: ```C-s auto-complete```
3. Install:
   - bring the cursor to the package "auto-complete"
   - type: 'i' and 'x'
4. Add the following lines to init.el:
```
(require 'auto-complete)
; do default config for auto-complete
(require 'auto-complete-config)
(ac-config-default)
```

## Install yasnippet
1. Find package:
   - type: M-x package-list-packages
2. Install yasnippet:
   - bring the cursor to the package "yasnippet"
   - type: 'i' and 'x'
3. Install yasnippet-snippets:
   - bring the cursor to the package "yasnippet-snippets"
   - type: 'i' and 'x'
4. Add the following lines to init.el:
```
(add-to-list 'load-path
   		(expand-file-name "~/.emacs.d/elpa/yasnippet-20180621.50/yasnippet"))

(require 'yasnippet)
(setq yas-snippet-dirs
     '("~/.emacs.d/mySnippets" 
	"~/.emacs.d/elpa/yasnippet-snippets-20180714.1322/snippets"
	))
(yas-global-mode 1)
```

## Install flymake-google-cpplint
1. Find package:
   - type: M-x package-list-packages
2. Install flymake-google-cpplint:
   - bring the cursor to the package "flymake-google-cpplint"
   - type: 'i' and 'x'
3. Install cpplint
   - ```$ sudo pip install cpplint```

4. Add the following lines to init.el:
```
; start flymake-google-cpplint-load
; let's define a function for flymake initialization
(defun my:flymake-google-init () 
  (require 'flymake-google-cpplint)
  (custom-set-variables
   '(flymake-google-cpplint-command "/opt/intel/intelpython3/bin/cpplint"))
  (flymake-google-cpplint-load)
)
(add-hook 'c-mode-hook 'my:flymake-google-init)
(add-hook 'c++-mode-hook 'my:flymake-google-init)
```

## Install flymake-cursor
1. Find package:
   - type: M-x package-list-packages
2. Install yasnippet:
   - bring the cursor to the package "flymake-cursor"
   - type: 'i' and 'x'

## Install google-c-style
1. Find package:
   - type: M-x package-list-packages
2. Install google-c-style:
   - bring the cursor to the package "google-c-style"
   - type: 'i' and 'x'


## Install markdown-mode
1. Find package:
   - type: M-x package-list-packages
2. Install markdown-mode:
   - bring the cursor to the package "markdown-mode"
   - type: 'i' and 'x'

## Install spell checker for emacs
1. Install aspell
(Mac) $ brew install aspell
(Linux) $ apt-get install aspell-en
2. Add the following lines to init.el:
```
;; Spell checker (aspell)
(setq ispell-program-name "/usr/local/bin/aspell")
```
