# Set up Scientific Programming Environment

### Tools I use
#### Programming languages
1. Python
2. Fortran
3. C++
### Editors
1. Spider for Python
2. Emacs   
(https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/Emacs/SetupEmacs.md)

### Set up the environment in Linux (Ubuntu)
- Go to https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/SetupScientificProgrammingEnv_Ubuntu.md

### Set up the environment in macOS
- Go to https://gitlab.com/hiromiyasuda/scientificprogrammingenvironment/blob/master/SetupScientificProgrammingEnv_Mac.md
